<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>
<body>
    <h1>tukar_besar_kecil</h1>
    <?php
        function tukar_besar_kecil($string)
        {
            $arrtemp='';
            
            for($i =0; $i<strlen($string); $i++)
            {
                if (ord($string[$i])<=90 && ord($string[$i])>=65 ) 
                {
                    $arrtemp .= strtolower($string[$i]);
                } 
                else if (ord($string[$i])<=122 && ord($string[$i])>=97) 
                {
                    $arrtemp .= strtoupper($string[$i]);
                } 
                else 
                {
                    $arrtemp .= $string[$i];
                }
            }
            return $arrtemp."<br>";

        }
            
            // TEST CASES
            echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
            echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
            echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
            echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
            echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

        
    ?>
</body>
</html>
